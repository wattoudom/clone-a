const CardData = [
  {
    title: "Free Shipping",
    img: "https://theveggiebox.com.au/wp-content/uploads/2020/03/4-pax-box.jpg",
    p: "Free on order over $300",
  },
  {
    title: "Free Shipping",
    img: "https://www.thespruceeats.com/thmb/d4-3wLGWdWQrdsYmcgOgokNDOxg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/vegan-tofu-veggie-burgers-recipe-3377169-hero-01-a2dd40a53b1c4d3ba21625925cc9e28b.jpg",
    p: "Free on order over $300",

  },
  {
    title: "Free Shipping",
    img: "https://cookingforpeanuts.com/wp-content/uploads/2021/05/Roasted-Chickpea-Veggie-Bowl.jpg",
    p: "Free on order over $300",

  },
  {
    title: "Free Shipping",
    img: "https://images.immediate.co.uk/production/volatile/sites/30/2021/07/Healthy-veggie-platter-688f617.jpg?quality=90&resize=556,505",
    p: "Free on order over $300",

  },
];

const postContainer = document.querySelector(".card-container");

const card = CardData.map(({ img, title, p }) => {
  return `
      <div class="col-lg-3 col-md-6 col-sm-12 p-3">
      <div class="card">
        <img
          class="card-img-top"
          src=" ${img} "
          alt="" />

        <div class="card-body">
          <h3> ${title} </h3>
          <p> ${p} </p>
        </div>
      </div>
    </div>
      `;
});

postContainer.innerHTML = card.join("");